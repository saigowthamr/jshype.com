import React from 'react'
import { searchStringInArray } from '../../../helper'

// function capitalizeFirstLetter(str) {
//   return str.charAt(0).toUpperCase() + str.slice(1)
// }

function MainUi(props) {
  const { allMarkdownRemark } = props.data

  const searchedCourse = searchStringInArray(
    `/${props.course}`,
    allMarkdownRemark.edges,
    props.limit ? props.limit : 5
  )

  return (

    <div>
      {props.logo && <span className="course-logo" >
        <img src={props.logo} style={{ margin: 0 }} alt={props.course} />
      </span>}


       {/* { <div className="ribbon"><span>New</span></div>} */}
      <div className={props.cls && props.cls}>
          {props.children}
        {!props.hide && <h1 style={{ textTransform:"capitalize"}}>{props.course}</h1>}
        <ul className="content-links">
          {searchedCourse.map(({ node }, i) => (
            <li key={i}>
              <a href={node.fields.slug}>{node.frontmatter.title}</a>
            </li>
          ))}

        </ul>
      </div>
      </div>


  )
}

export default MainUi
