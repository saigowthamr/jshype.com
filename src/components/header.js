import React from 'react'
import Link from 'gatsby-link'
import './header.css'
import GithubIcon from 'react-icons/lib/go/mark-github'


class Header extends React.Component {
  state = {
    active: false,
  }

  openUp = () => {
    this.setState({
      active: !this.state.active,
    })
  }

  render() {
    const { location, siteTitle } = this.props
    return (
      <div className="main-nav">
        <button onClick={this.openUp} className="navbtn">
          {this.state.active ? 'Show' : 'Hide'}
        </button>

        <div className="header">
          {!this.state.active && (
            <Link to="/" className={`logostyle`}>
              {siteTitle}
            </Link>
          )}

          <nav
            className={this.state.active ? 'nav' : 'nav active'}
            aria-hidden={this.state.active ? true : false}
          >
            <Link
              to={'/reactjs/tutorial/'}
              activeStyle={{ backgroundColor: '#f5f5f5' }}
            >
              React
            </Link>
            <Link
              to={'/angular/tutorial/'}
              activeStyle={{ backgroundColor: '#f5f5f5' }}
            >
              Angular
            </Link>
            <Link
              to={'/vuejs/tutorial/'}
              activeStyle={{ backgroundColor: '#f5f5f5' }}
            >
              Vue
            </Link>
            <Link
              to={'/javascript/tutorial/'}
              activeStyle={{ backgroundColor: '#f5f5f5' }}
            >
              JavaScript
            </Link>
          </nav>

          <a
            className="github"
            href="https://github.com/saigowthamr"
            target="_blank"
            rel="noopener noreferer"
            style={{ color: 'black' }}
            title="GitHub"
          >
            <GithubIcon style={{ verticalAlign: `text-top` }} />
          </a>
        </div>
      </div>
    )
  }
}

export default Header
