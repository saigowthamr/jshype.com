import React from 'react';
import Link from 'gatsby-link'
import './footer.css';
import './header.css'


function Footer({siteTitle}){

    return (
        <footer className="footer-g">
            <hr />

            <section className="footer-start">
                <Link to="/" className="footer-title logostyle">
                    {siteTitle}
                </Link>

                <nav className="footer-links">

                        <Link to={"/reactjs/tutorial/"} activeStyle={{ backgroundColor: "#f5f5f5" }}>React</Link>
                        <Link to={"/angular/tutorial/"} activeStyle={{ backgroundColor: "#f5f5f5" }}>Angular</Link>
                        <Link to={"/vuejs/tutorial/"} activeStyle={{ backgroundColor: "#f5f5f5" }}>Vue</Link>
                    <Link to={"/javascript"}>JavaScript</Link>

                </nav>
             </section>
         </footer>
     )

}

export default Footer;