import React from 'react'
import './main.css'
import reactLogo from '../img/reactlogo.svg'
import js from '../img/jslogo.png'
import vue from '../img/vuelogo.png'
import ang from '../img/anglogo.png'
import gats from '../img/gatsbylogo.jpeg';
import MainUi from '../components/mainui/mainui'


class Index extends React.Component {
  render() {
    return (
      <div className="bglight auto">
        <div className="main-grid padding-top5">
          <MainUi course={`javascript`} logo={js} data={this.props.data} />
          <MainUi course={`vuejs`} logo={vue} data={this.props.data} />
          <MainUi course={`reactjs`} logo={reactLogo} data={this.props.data} />
          <MainUi course={`angular`} logo={ang} data={this.props.data} />
          <MainUi course={`gatsbyjs`} logo={gats} data={this.props.data} />
        </div>
      </div>
    )
  }
}
export default Index

export const query = graphql`
  query Evry {
    allMarkdownRemark(
      # filter: { fields: { slug: { regex: "*/javascript/" } } }
      sort: { fields: [frontmatter___myid], order: ASC }
    ) {
      edges {
        node {
          frontmatter {
            title
          }
          fields {
            slug
          }
        }
      }
    }
  }
`
