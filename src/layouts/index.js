import React from 'react'
import Metapost from '../components/MetaPost'
require('prismjs/themes/prism.css')
import Header from '../components/header';
import Jsimg from '../img/js.jpg'
import Footer from '../components/footer'
import './index.css'

const Layout = ({ children, data, location }) => (
  <div>
    <Metapost
      title={data.site.siteMetadata.title}
      description={data.site.siteMetadata.description}
      url={data.site.siteMetadata.siteUrl}
      thumbnail={data.site.siteMetadata.siteUrl+Jsimg}
      stop
    />

    <Header siteTitle={'Js hype'} location={location} />
    <div>
      {children()}
    </div>

    <Footer siteTitle={'Js hype'} />
  </div>
)


export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
        description
        siteUrl
      }
    }
  }
`
