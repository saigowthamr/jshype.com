import React from 'react'

import CommonMarkdown from '../components/commonmarkdown'

function AngularMarkdown(props) {
  const { course } = props.pathContext
  return (
    <CommonMarkdown
      data={props.data}
      course={course.slice(1)}
      location={props.location}
      pathContext={props.pathContext}
    //   thumbnail={angularImg}
    //   clogo={anglogo}
    />
  )
}

export default AngularMarkdown

export const query = graphql`
  query AngularFucker($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      timeToRead
      birthTime
      modifiedTime
      frontmatter {
        title
        description
        date(formatString: "MMM Do")
        code
        logo
        thumbnail
      }
      fields {
        slug
      }
    }

    site {
      siteMetadata {
        url
        author
      }
    }

    allMarkdownRemark(
      sort: { fields: [frontmatter___myid], order: ASC }
    ) {
      edges {
        node {
          frontmatter {
            title
          }
          fields {
            slug
          }
        }
      }
    }
  }
`
