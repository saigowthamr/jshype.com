---
title: Should I learn JavaScript or Python
description: "JavaScript is universally useable language had got more popular in current year so beginner's got in confusion to choose which language is popular."
date: "2018-08-25"
thumbnail:  "./images/js.jpg"
tags: ['javascript']
---

Both javascript and python are  programmming languages built
for the different purposes to solve different type of problems.


What is your end goal you need to achieve by learning one of those programming languages.

 - Like in general if you learn engilsh then you should comfortable to  speak with you international friends or clients.


## Why JavaScript ?

- Easy to learn for the beginners.
- Large community.
- Job demand is increasing.

JavaScript is orginal built for the purpose of the  web pages  needs to interact with the users.

But now JavaScript is used everywhere from servers to the browsers,Mobile apps ,Machine learning,Ai ,even in space.



## Why python ?

 - Python is majorly used in the Data science and Machine Learning,automation.
 - Easy syntax.
 - Job demand is increasing.

In web developent python is used in server side programming.



According to stackoverflow survey JavaScript is the most popular used technology around the globe.
Check out the <a href="https://insights.stackoverflow.com/survey/2018/">Stackoverflow 2018 survey</a>


So find your purpose or end goal why you should learn.

My final suggestions it is hard to learn anything in first often you get confused in middle.Don't feel
bad it happens to everyone so choose one of the programming language and stick with it untill you feel
comfortable and never giveup.


