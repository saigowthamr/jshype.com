---
myid: 16
date: '2018-08-14'
title: Build a website
description: 'we are building a sample website using vuejs and webpack ,babel cli 3
it contains header ,body,footer components'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---

Let's Build a simple website Using our Knowledge .

Our website have header component,body and footer component.

## How to intialize a new project ?

- [check  out here](/vuejs/creatingproject)


### Header component

In you components folder create a Header.vue.

```javascript
<template>
    <header class="header">
        <h1>{{name}}</h1>
    </header>
</template>



<script>
export default {
    data(){
        return{
            name:"My simple website"
        }
    }
}
</script>


<style scoped>
.header{
    height: 4rem;
    background-color: aliceblue;
}

</style>
```
---
scoped means styles are only accessible inside the head component.



### Body Component.

Create a Body.vue in components folder

```javascript
<template>
    <div class="body">
<div class="item">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Illum omnis,
     beatae maiores ex nihil rerum odit a voluptates quidem in eveniet quas natus illo
    nemo? Alias repellendus a soluta sapiente!</div>
    <div class="item">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Illum omnis,
     beatae maiores ex nihil rerum odit a voluptates quidem in eveniet quas natus illo
    nemo? Alias repellendus a soluta sapiente!</div>

    </div>
</template>


<script>
export default {

}
</script>


<style scoped>

.body{
    margin-top: 2rem;
    padding: 1rem;
    display: flex;
    flex-wrap: wrap;
}
.item{
    padding: 1rem;
    width: 50%;
}
</style>
```
---

Now last and final which is Footer Component.

### Footer Component.

```javascript
<template>
    <footer>
        <ul>
            <li>Contact us</li>
            <li>Address</li>
        </ul>
    </footer>
</template>


<script>
export default {

}
</script>


<style scoped>

footer{
    height: 5rem;
    margin-top: 3rem;
    background-color: black;
    color: white;
    padding: 1rem;
}
</style>

```

Now open App.vue and import Above three Components.

App.vue

```javascript
<template>
    <div class="main">
     <Header></Header>
      <Body></Body>
       <Body></Body>
      <Footer></Footer>
    </div>
</template>


<script>
import Header from './components/Header';
import Body from './components/Body';
import Footer from './components/Footer';
export default {
    data(){

    },
    components:{
        Header,
        Body,
        Footer
    }
}
</script>


<style>
*{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}
.main{
    height: 100vh;
    background-color: rgb(243, 243, 243);
}

h1{
    text-align: center;
}
</style>

```
---

Thats it we are successfully created our first website.


