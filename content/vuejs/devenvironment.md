---
myid: 14
date: '2018-08-13'
title: Dev environment setup
description: 'Development setup for vuejs 2 using webpack it hepls to bundle all
files we are using vue cli to create a vue boiler plate'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---

Its time to setup a development environment using vue-cli.so far we are learning
uses and some important concepts of vuejs.


## Requirements

 - Nodejs.

 Open your terminal and run below commands

 ```bash
npm install -g @vue/cli

#for yarn

yarn global add @vue/cli
 ```
Once you completed above step.

Check out version  using `vue --version` if it shows verison number then you are successfully installed.



