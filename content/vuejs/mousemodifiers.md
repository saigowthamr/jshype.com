---
myid: 8
date: '2018-08-11'
title: Mouse Modifiers
description: 'Mouse  modifiers for the right click,left click and middle click
how to respond the mouse events'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---


## Vuejs also offers mouse modifers

- .right: handles the mouse right click
- .middle: handles the mouse middle button click
- .left : handles the mouse left click


Example:
<iframe height='265' scrolling='no' title='Mouse button Modifiers' src='//codepen.io/saigowthamr/embed/preview/LBqebN/?height=265&theme-id=light&default-tab=js,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/LBqebN/'>Mouse button Modifiers</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>

In above example we disabled the mouse right click on red border outside
the red boder mouse right click works just fine.