---
myid: 10
date: '2018-08-11'
title: Form Handling
description: 'Learn Form handling using v-model which is two data binding offers by vuejs it used to update the both state and view at the same time'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---

Vuejs offers us <mark>v-model</mark> directive which helps to do two data binding if you come from angular it is same like ng-model in angular.

## Two way data binding

- It updates the both state and view.


#### v-model usage in input element.

```javascript
<input type="text" v-model="name" />
```

<iframe height='265' scrolling='no' title='Form handling - Vuejs' src='//codepen.io/saigowthamr/embed/preview/PBVvPy/?height=265&theme-id=light&default-tab=html,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/PBVvPy/'>Form handling - Vuejs</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>




some modifiers that we can use with v-model directive.


1. **lazy** : .lazy modifier is used to update the view lazily.
2. **number** : .number is used to convert the string type to the number type.
3. **trim** : .trim is used to trim the lead and trailing spaces from the text.


usage of modifiers.

<iframe height='265' scrolling='no' title='v-model directive modifiers' src='//codepen.io/saigowthamr/embed/preview/YjBbpL/?height=265&theme-id=light&default-tab=html,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/YjBbpL/'>v-model directive modifiers</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>



