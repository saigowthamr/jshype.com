---
myid: 3
date: '2018-08-11'
title: Conditional rendering
description: 'how render conditionally using v-if directive hides and removes the elements upon conditonals  beginner friendly tutorials'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---

## How render Conditionally in vuejs?


<iframe height='265' scrolling='no' title='Vuejs Conditional rendering' src='//codepen.io/saigowthamr/embed/preview/QBYpqz/?height=265&theme-id=light&default-tab=html,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/QBYpqz/'>Vuejs Conditional rendering</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>

If any attributes that start   `v-`  that are offered by vuejs and they called as a
directives in Vuejs.


we used v-if directive to render condtionally.so in above code we only render dom elements if show property is true but in h2 element we used <mark>!show</mark> it means false so it didn't render in the dom.


