---
myid: 15
date: '2018-08-13'
title: Creating a Project
description: 'By using vue-cli we are creating a project with webpack and babel
pre added template provied by vue'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---

In last page we installed [Vue-cli](/vuejs/devenvironment).

Now its time to create a  project using vue-cli. Vue offers us different types of templates we are using vue-webpack-boilerplate for this project.

Open your terminal a run

```javascript
npm install -g @vue/cli-init
vue init webpack my-vue   // intializing the boiler plate in my-vue folder
cd my-vue // change the directory to my-vue
npm run dev // to start dev server
```

Open my-vue folder in your favourite code editor.

it looks  the below image.


 <img src="../../img/vuefolder.png" alt="vue js folder structure"/>



If you are using Vscode install vetur extension it helps for highlighting code and auto complete

Delete App.vue file and  components folder because we are writing it from scratch.


 Now create a  App.vue file.

 They are three parts we  need to declare in vue components.

 - first part is template.

 - second part is script tag it means javascript things goes here.

 - third one is style tag  where we can add our styles.


App.vue code

```javascript
        <template>
            <div class="main">
            <h1>{{name}}</h1>
            </div>
        </template>


        <script>
        export default {
            data(){
                return{
                    name:"My first vue project"
                }
            }
        }
        </script>


        <style>
        .main{
            height: 100vh;
            background-color: rgb(255, 239, 224);
        }

        h1{
            text-align: center;
        }
        </style>

```
---

Now check in your browser you will  see My first vue project  is displayed with hot reloading it means you can see the instant changes.


Instead of  creating separate files for css ,javascript in vue we put everthing in single file.


If you open your main.js in the same folder you will see the import of App component.

```javascript
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
});

```

Here We are declared the Vue instance and  targeting id app in html file.

- In components property *App* component is added which is a short hand syntax
 of {App:App}
- In template property we added <App/> template.

