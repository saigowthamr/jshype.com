---
myid: 17
date: '2018-08-16'
title: Comments
description: 'How to write comments in vuejs with examples beginner friendly tutorials of vue'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---


## How to write comments in Vuejs?


```html

<template>
<!--<h1>This is a comment</h1>-->
</template>

<script>
 //comments
</script>

```