---
myid: 6
date: '2018-08-11'
title: Event Modifiers
description: 'how  event modifiers ,event propagation and event.preventDefault prevent the browsers default behaviour'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---


We often sometimes want to stop the
browsers default behaviour of handling
events for that vuejs offers us different types of modifiers.

## How To stop the browser default loading behaviour when we submit a form?

```javascript
<form v-on:click.prevent="submit"></form>
```

Like how we used event.preventDefault in plain javascript.

---

for event.stopPropagation

```javascript
<button v-on:click.stop="onHit"></button>
```
---
**once modifer only excutes the handler function once.**

```javascript
<button v-on:click.once="oneHit"></button>
```
---
####  Short hand syntax for registering event handlers

```javascript
//long hand
<button v-on:click="oneHit"></button>
//short hand syntax
<button @click="oneHit"></button>
```
both does the same work.
we used shorthand syntax in below example

<iframe height='265' scrolling='no' title='Event modifiers in Vuejs' src='//codepen.io/saigowthamr/embed/preview/rrPYKQ/?height=265&theme-id=light&default-tab=js,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/rrPYKQ/'>Event modifiers in Vuejs</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>