---
myid: 4
date: '2018-08-11'
title: For Loops
description: 'how  conditionals and loops works v-for directive and iterate over the
list of items'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---


## How to use v-for directive in vuejs?

Some times we have a list of items we need to iterate for that vuejs offers us
`v-for` directive which helps to iterate over list of items.


<iframe height='265' scrolling='no' title='v-for directive Vuejs' src='//codepen.io/saigowthamr/embed/preview/GBzWBx/?height=265&theme-id=light&default-tab=js,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/GBzWBx/'>v-for directive Vuejs</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>



