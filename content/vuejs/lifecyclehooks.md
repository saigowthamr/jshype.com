---
myid: 13
date: '2018-08-13'
title: Lifecycle Hooks
description: 'We can discuss about life cycle hooks such are created , mounted,unmounted, updated,destroyed are fired at different stages of instance creation updating dom elements remove listeners from the dom'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---

Vuejs provides us different type of lifecycle hooks which are
invoked at the different point of  time such as instance creation ,updation of the state,cleaning the dom listeners.

Check out [Lifecycle Diagram](https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram).


- **beforeCreate**:Invoked after instance is intialized.
- **created**:Invoked after instance is created it is best place to make
ajax requests.

- **beforeMount**:Invoked  right before the mounting begins .

- **mounted**:Invoked when instace is connected to the dom.


- **beforeUpdate**:Invoked before the update happens in the dom.

- **updated**:Invoked after the update happens.

- **beforeDestroy**:Invoked before instance is destroyed.
- **destroyed**: Invoked after a Vue instance is destroyed,event listeners are removed and its child instances are also destroyed best place to clear cookies or any type of local storage.


<iframe height='265' scrolling='no' title='Life cycle hooks vuejs' src='//codepen.io/saigowthamr/embed/preview/jpJMPx/?height=265&theme-id=light&default-tab=js,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/jpJMPx/'>Life cycle hooks vuejs</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>

