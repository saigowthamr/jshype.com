---
myid: 7
date: '2018-08-11'
title: Key Modifiers
description: 'Key  modifiers and  key bindings works in vuejs  enter key esc key,
tab , down ,right space keys etc'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---

Sometimes we need to respond the keyboard events of the user for that
vue offers some key modifiers.

Let's see key modifiers in action.

## Enter key modifer
```javascript
<input type="text" @keyup.enter="submit"/>
```
---
## Esc key modifer

```javascript
<input type="text" @keyup.esc="submit"/>
```

- .enter: handles the enter key event
- .esc : handles the esc key event
- .tab : handles the tab key event
- .delete: handles the both delete key and backspace
- .space: handles the space key event
- .up: handles the up key event
- .down: handles the down key event
- .left: handles the left key event
- .right: handles the right key event

---


<iframe height='265' scrolling='no' title='Key modifiers in Vuejs' src='//codepen.io/saigowthamr/embed/wxNPLP/?height=265&theme-id=light&default-tab=html,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/wxNPLP/'>Key modifiers in Vuejs</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>