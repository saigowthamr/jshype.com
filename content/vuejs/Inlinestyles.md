---
myid: 12
date: '2018-08-12'
title: Inline styles
description: 'How to style vue components using inline styles , dynamically add and remove inline styles in templates'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---


## How to dynamically add and remove inline styles?


```html
<div id="root">
  <div :style=`color:${active?"green":"red"}`>
    <p>
     No pain no gain
    </p>
  </div>
  <button @click="change">Change color</button>
</div>
```


```javascript
new Vue({
  el:"#root",
  data:{
    active:true,
  },
  methods:{
    change:function(){
      this.active=!this.active
    }
  }
})
```
We are using v-bind:style (short hand :style) to dynamic add and remove styles.

When active property is true we are adding green color otherwise red color.

Live example:

<iframe height='265' scrolling='no' title='Inline styles Vuejs' src='//codepen.io/saigowthamr/embed/preview/VBRjrJ/?height=265&theme-id=light&default-tab=html,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/VBRjrJ/'>Inline styles Vuejs</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>


### Using Object syntax

```html
<div id="root">
    <div :style="styleObject">
    <p>
     No pain no gain
    </p>
  </div>

     <div :style="{color:green,border:greenBorder}">
    <p>
     No pain no gain
    </p>
  </div>
  <button @click="change">change</button>
</div>
```
---
You can directly pass styles in as a object.


<iframe height='265' scrolling='no' title='Inline styles Vuejs' src='//codepen.io/saigowthamr/embed/preview/oMVLaK/?height=265&theme-id=light&default-tab=js,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/oMVLaK/'>Inline styles Vuejs</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>
