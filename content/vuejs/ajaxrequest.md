---
myid: 14
date: '2018-08-13'
title: Ajax Request
description: 'How to make ajax/http request in vue instance
and which lifecycle method is best place to make ajax request vuejs 2'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---

Some times we need to make a ajax request in Vuejs at the time of instance creation like getting the feed from the backend server.


In below example we are using <mark>created</mark> lifecycle hook provided by the vuejs .

```javascript
  created:function(){
    fetch('https://jsonplaceholder.typicode.com/users').
    then(res=>res.json())
    .then(data=>{
      this.persons=data
    })
    .catch(err=>console.log(err))
  }
```

```html
<div id="root">
  <ul v-for="person in persons">
    <li>Name : {{person.name}}</li>
  </ul>
</div>
```
---

### Live exmaple of making ajax request using fetch api.

<iframe height='265' scrolling='no' title='Ajax Request Vuejs' src='//codepen.io/saigowthamr/embed/preview/YjgNwg/?height=265&theme-id=light&default-tab=js,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/YjgNwg/'>Ajax Request Vuejs</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>