---
myid: 5
date: '2018-08-11'
title: Event Handling
description: 'how  event handling works v-on:click  directive binding and event listeners to the dom elements vue js events'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---





## How to add Event listeners  ?

- v-on directive is used  to add event listeners to the dom elements.
- Vue offers us a property called methods.
- Inside the methods property we need to create the event handler functions.


<iframe height='265' scrolling='no' title='Event handling in Vuejs' src='//codepen.io/saigowthamr/embed/preview/bjzYVw/?height=265&theme-id=light&default-tab=js,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/bjzYVw/'>Event handling in Vuejs</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>

