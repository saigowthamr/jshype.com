---
myid: 2
date: '2018-08-11'
title: Getting Started
description: 'Introducing Vue constructor and el ,data property.How it works showing
in plain javascript using vuejs cdn links'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---


##Vuejs

We are using a plain html and cdn links to setup a basic workflow


```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Getting Started Vuejs</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>
<body>
    <div id="root">
        {{hello}}
    </div>

   <script>
     var app = new Vue({
         el:"#root",
         data:{
            hello:"Getting started with vue"
         }
     })

   </script>
</body>
</html>
```
---

### How above code works?

1. We included a cdn link of vuejs
2. created a div element with id root.
3. script tag with Vue constructor.


**el**: It means which element Vue needs to  target on dom.

**data**: Internal state that vue manages.

At final we added  {{hello}} inside the root element because vue used double curly braces for data binding.

That's it  we are successfully created our first vuejs app.


<iframe height='265' scrolling='no' title='Vuejs Getting Started' src='//codepen.io/saigowthamr/embed/preview/KBJWgb/?height=265&theme-id=light&default-tab=html,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/KBJWgb/'>Vuejs Getting Started</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>


