---
myid: 11
date: '2018-08-12'
title: Styling
description: 'How to style vue components using class attributes,inline styles , dynamically add and remove class names'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---


As we know vuejs used template base syntax it means we can use class attributes
like how we used in html5.


### Class names

```html
<div class="box">
 <p>
 Hello vuejs you are using class attributes to style me
 </p>
</div>
```

### Inline styles


```html
<div style="border:1p solid black">
 <p>
 Hello vuejs you are using class attributes to style me
 </p>
</div>
```
---

###  How to dynamically add and remove class names?

<iframe height='265' scrolling='no' title='Dynamically add class Vuejs' src='//codepen.io/saigowthamr/embed/preview/xJMNaY/?height=265&theme-id=light&default-tab=html,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/xJMNaY/'>Dynamically add class Vuejs</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>


- In above example we used template strings <mark>\`\`</mark> its just a javascript
thing.

- To handle the dynamic things we need to use :class instead of normal class attribute. :class helps to add and remove  class names dynamically.

- :class is short hand property for v-bind:class.


By  toggling the button we are changing the active property value to the true or false.

rendered   output element looks like

```javascript
// active is true
<div class="red"></div>

//active is false
<div class="green"></div>
```

**Similar Example using objects.**

<iframe height='265' scrolling='no' title='Dynamically Vuejs class' src='//codepen.io/saigowthamr/embed/preview/PBVrGq/?height=265&theme-id=light&default-tab=html,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/saigowthamr/pen/PBVrGq/'>Dynamically Vuejs class</a> by saigowtham (<a href='https://codepen.io/saigowthamr'>@saigowthamr</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>

- In above code we are using an object where left hand side is class name and right handside is active property.it will add green class when active is true.

- This object type of syntax is showed in Vuejs documentation.

rendered   output element looks like

```javascript
// active is false
<div class="red"></div>

//active is true
<div class="red green"></div>
```


You can use any of them both works fine.