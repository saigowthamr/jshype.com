---
myid: 1
date: '2018-08-11'
title: Introduction
description: 'Learn vuejs  in easy way vuejs is a javascript framework which is used to make single page apps and multipage apps and it uses the template based syntax.In mvc model vue concentrates  on the view model'
logo: /img/vuelogo.png
thumbnail: /img/vuejs.png
---

# What is Vuejs ?

- Vuejs is a popular javascript framework used to make user interfaces.It is mainly concentrated on the view layer in the Mvc model you can also create single page apps
and mulitpage apps using Vuejs.

- Vuejs is created by the Evan you.

## Advantages

 - Very Performant because it is just 20KB min+gzip.
 - Easy To start
 - Virtual Dom
 - Easy to integrate.
 - Progressive framework.

## Who uses Vuejs ?

- Adobe
- Alibaba
- Xiaomi
- [Gitlabs](https://about.gitlab.com/2017/11/09/gitlab-vue-one-year-later/)

Many more..