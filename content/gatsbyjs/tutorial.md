---
myid: 1
title: Introduction
date: '2018-08-19'
description: 'Gatsbyjs is a static site generator used to generate static web pages
it is built on top of reactjs one of its keyfeatures  is blazing fast'
logo: /img/gatsbylogo.jpeg
thumbnail: /img/gatsby.jpeg
---


Gatsby is a Static Generator for React By using Gatsby you can build
any type of websites or Progressive web apps.


## Why Gatsby?

- Gatsby has a rich plugin ecosystem.
- Code is fully optimized.
- Future proof Jamstack websites.
- Everything is static.
- Code splitting out of the box.






