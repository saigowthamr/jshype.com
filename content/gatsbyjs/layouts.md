---
myid: 4
title: Layouts
date: '2018-08-19'
description: "Layouts helps us to create the  simple layouts often that can share over all pages in gatsbyjs "
logo: /img/gatsbylogo.jpeg
thumbnail: /img/gatsby.jpeg
---

Let's install a new starter provided by the gatsby.

Open your terminal and run below commands.

```bash
gatsby new my-starter
cd my-starter // changing the directory to my-starter
gatsby develop
```

It might looks like below image.

![gatsby layouts](../../img/layouts.png)


Open the  project folder in your favorite code editor have you seen layouts and components folder is there inside the src folder.

If you define anthing inside the layouts file it might be shared across to the all pages.

If you have any shared feature that might be used across in your website you can
create a components for that example buttons, header, footer.


Open layouts file

```javascript
const Layout = ({ children, data }) => (
  <div>
    <Header siteTitle={data.site.siteMetadata.title} />
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '0px 1.0875rem 1.45rem',
        paddingTop: 0,
      }}
    >
      {children()}
    </div>
  </div>
)
```

There is a header component which is imported from the components folder.

children  property : everthing you defined inside the pages folder goes here.

