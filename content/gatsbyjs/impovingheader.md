---
myid: 5
title: Nav bar
date: '2018-08-19'
description: "We are creating a header component with navigation bar  in this gatsby beginners tutorial"
logo: /img/gatsbylogo.jpeg
thumbnail: /img/gatsby.jpeg
---

In this tutorial we are adding a nav bar to our gatsby site.

Create a contact us page .

```javascript
import React from 'react'
import Link from 'gatsby-link'

const ContactUs = () => (
    <div>
        <h1>This contact us page</h1>
    </div>
)

export default ContactUs

```
Now open your header.js file in components folder.

```javascript
const Header = ({ siteTitle }) => (
  <div
    style={{
      marginBottom: '1.45rem',
    }}
  >
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '1.45rem 1.0875rem',
      }}
    >
      <nav>
        <ul style={{display:"flex",justifyContent:"space-between"}}>
          <li>
          <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/page-2">Page 2</Link>
          </li>
          <li>
            <Link to="/contactus">Contact us</Link>
          </li>
      </ul>
      </nav>

    </div>
  </div>
)

export default Header
```


That's it we are succesfully added a nav bar to our website.


![nav bar gatsbyjs](../../img/gnavbar.png)
