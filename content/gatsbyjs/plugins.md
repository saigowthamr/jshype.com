---
myid: 7
title: Plugins
date: '2018-08-20'
description: "Plugins helps us to enable advanced features in gatsbyjs like quering data from graphql and programatically creating pages"
logo: /img/gatsbylogo.jpeg
thumbnail: /img/gatsby.jpeg
---

In this tutorial we learn about how to add plugins to the gatsby site.

## How to add plugins to the gatsby site?

  Gatsby has a rich plugin ecosystem where you can find different type of
  plugins according to your usage.


Now let's see in action .

if you open gatsby.conifg.js file you will see the plugins array.Anytime we install a new plugin we need to add the plugin name to the array.


First we need to install the plugins.

```bash
npm install --save gatsby-plugin-page-creator
```

Once  installed  we need to add it in the gatsby.config.js

```javascript
module.exports = {
  siteMetadata: {
    title: 'Gatsby Default',
  },
  plugins: [
    {
      resolve: `gatsby-plugin-page-creator`,
      options: {
        path: `${__dirname}/src/my`,
      },
    },
  ],
}
```
Hoo we successfully added page creator plugin.