---
myid: 8
title: Quering data
date: '2018-08-20'
description: "Gatsbyjs uses the graphql behind the scenes by using we can easily query the data suppose we have site url which need to shared all pages we only defined it in the one place and reuse it again by using the graphql"
logo: /img/gatsbylogo.jpeg
thumbnail: /img/gatsby.jpeg
---


## How to query the data using graphql ?

If we run a gatsby develop two urls are available one is localhost:8000 and other is localhost:8000/___graphql. Open this url in your browser have you seen
a Graphiql user interface if you are not familar with graphql
checkout howtographql.com.


If you open gatsby.config.js there is siteMetadata object is available we can query that data using graphql.

```javascript
module.exports = {
  siteMetadata: {
    title: 'My first site',
  },
  plugins: [
    {
      resolve: `gatsby-plugin-page-creator`,
      options: {
        path: `${__dirname}/src/my`,
      },
    },
  ],
}

```


![graphql quering in gatsbyjs](../../img/graphqlgat.png)


Let's see how we can use the title.If we add a graphql query to the any of the pages the data will be available in the props.data property.

layouts/index.js

```javascript
import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Header from '../components/header'
import './index.css'

const Layout = ({ children, data }) => (
  <div>
    <Header siteTitle={data.site.siteMetadata.title} />
    <div>
      {children()}
    </div>
  </div>
)


export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
```

<img style="border:1px solid black" alt="gatsby query data" src="../../img/graphqlgat2.png"/>


