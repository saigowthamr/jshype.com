---
myid: 3
title: Routing
date: '2018-08-19'
description: "Routing in gatsbyjs if you create a file in pages folder it automatically creates the routing by gatsby "
logo: /img/gatsbylogo.jpeg
thumbnail: /img/gatsby.jpeg
---


In this tutorial we learn about how to do routing in gatsbyjs.


Open your project in your favorite code editor.

![gatsby folder structure](../../img/gatsbyfolder.png)

In your pages folder  index.js file refers to the home route in gatsby it means (/).

If you create a new file in pages folder gatsby automatically creates the route.

Let's create one.

*Create a second.js file in your pages folder.*

```javascript
import React from "react"

function Second() {

        return (
            <div>
                <h1>Second page</h1>
            </div>
        )
    }

export default Second;
```

If you open localhost:8000/second you would see the second page.


In your index.js file  import the gatsby-link

```javascript
import React from "react"
import Link from 'gatsby-link';

class Index extends React.Component{

    render() {
        return (
            <div>
                <h1>Home Page</h1>
                <Link to="/second">Second page</Link>
            </div>
        )
    }
}

export default Index;
```

It is a Link component provided by the gatsby to means we need to refer the route.

In our pages folder we already created a second.js file so that we added (/second).

<img style="border:1px solid black" alt="routing gatsby" src="../../img/routing-gatsby.png"/>