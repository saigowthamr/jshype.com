---
myid: 6
title: Styling
date: '2018-08-19'
description: "In this tutorials you will learn about how to add inline styles and external stylesheets to gatsbyjs"
logo: /img/gatsbylogo.jpeg
thumbnail: /img/gatsby.jpeg
---


If you are familiar with the react styling  you can skip this tutorial.

Alright no problem if you don't know how to style we will walk through now
step by step.


## How to add internal styles?

```javascript
function Newpage(){
     return (
         <div style={{display:"flex",backgroundColor:"red"}}>
         <h1 style={{textAlign:"center"}}>This is heading</h1>
         </div>
     )
}
```

we need to pass styles as a object.


## How to add external stylesheets?

```javascript
import './btn.css';
function Button(props){
    return (
        <button className="btn">{props.name}</button>
    )
}

```

We need to add className instead of class

If you want to know more about styling refer [styling in react](/reactjs/styling/).