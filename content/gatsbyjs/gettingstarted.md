---
myid: 2
title: Getting Started
date: '2018-08-19'
description: "Getting started with gatsbyjs by using it's cli and learn how to build static sites"
logo: /img/gatsbylogo.jpeg
thumbnail: /img/gatsby.jpeg
---

In this tutorial we  learn about how to install gatsbyjs cli and setup a new project.


## Requirements

 1. Make sure Nodejs is installed in your pc.
 2. Familiar with command prompt.
 3. Basic knowledge of react is required.If you don't know check out [React Tutorials](/reactjs/tutorial/)



Now open your terminal and run below commands.

```bash
npm install --global gatsby-cli
```
If you are using mac use sudo infront of npm.


#### Setup a new  project using cli

```bash
gatsby new gatsby-site https://github.com/gatsbyjs/gatsby-starter-hello-world
```

```bash
cd gatsby-site
gatsby develop // to fire the dev server
```

In your browser open localhost:8000


<img style="border:1px solid black" alt="gatsbyhelloworld" src="../../img/gatsbyhello.png"/>