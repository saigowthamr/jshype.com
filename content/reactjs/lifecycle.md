---
myid: 10
title: Lifecycle Methods
date: '2018-08-06'
description: 'Lifecycle methods helps  us to make some changes to the code at time
of mounting,updating and unmounting to the dom'
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---


Everything has a birth and death in the real world like the samething
in react components have birth and death.

### componentDidMount():
- This method is invoked  after  the component is mounted to the dom tree this a good place to make network requests.


### shouldComponentUpdate(nextProps,nextState):
- This method is invoked before an update occurs whether the update is needed or not  By default it returns false. This method is not called for the initial render or when forceUpdate() is used.

### componentDidUpdate():
- This method invoked  after a component is updated and also it
is not invoked on intial render.

`componentDidUpdate() is not invoked if shouldComponentUpdate()return false`


### componentWillUnmount() :
This method is invoked just before a component is unmounted from the DOM.This is the ideal place to perform cleanup and canceling network requests , clearing timers.