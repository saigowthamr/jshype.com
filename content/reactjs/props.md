---
myid: 4
date: '2018-08-05'
title: Props
description: 'Props helps us to pass the data to the child components .props are only readable in react'
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---


Props helps us to pass the data to the child components or we can contact
child components by using props.


# How to use Props in React ?

 ###Stateless Components

```javascript
function Boo(props){

    return <div>{props.name}</div>
}

```


### Statefull Components
```javascript
class Boo extends React.Component{
    constructor(props){
      super(props);
      this.state = {name:"from state"}
    }

    render(){
        return <div>{this.props.id} {this.state.name}</div>
    }
}

```

In Statefull Components we need to pass the props to the constructor method.
where this keyword refers to the current component.




### Uses

- Props are Immutable
- By using the props you can easily pass data to the child components.


