---
myid: 13
title: Build a Website
date:  '2018-08-08'
description: 'How to build a website using reusable header ,footer,body components
using reactjs library'
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---

So far we are just learning what react offers to us but we can't build anything.It's time to build a simple website by using the  React.

Simple means just simple page.

- Hope our website should have a header , body and footer.


let's start with a Header component

### Header Component
---
```javascript
function Header(props) {
  return (
    <header style={{
    backgroundColor:"green",
      padding: "1rem",
     border: "1px solid" }}>
      <h1>{props.title}</h1>
    </header>
  );
}
```
---

## Body Component
---
```javascript
function Body() {
  return (
    <div style={{ display: "flex",
      backgroundColor: "yellow",
      padding:"1rem",
     justifyContent: "center" }}>
      <div>
        <h1>My first Heading</h1>
        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum
        </p>
      </div>
      <div>
        <h1>My second Heading</h1>
        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum
        </p>
      </div>
    </div>
  );
}
```
---
## Footer Component

```javascript
function Footer(props) {
  return (
    <footer style={{backgroundColor:"orange",padding:"1rem"}}>
      <h1>My footer</h1>
      <ul>
        <li>{props.name}</li>
        <li>{props.number}</li>
        <li>{props.address}</li>
      </ul>
    </footer>
  );
}
```
---

So far we build a individual components by using the  all individual components we need to
build a final website.


```javascript
class App extends React.Component {
  render() {
    return (
      <div style={{backgroundColor:"#ccc",height:"100vh",padding:"1rem"}}>
        <Header title={"Simple web"} />
        <Body/>
        <Footer name={'test'} number={12445}
        address={'1/983094 - ooo backside'}
        />
      </div>
    );
  }
}
```
---
Thats it we are successfully completed.

[Website Demo Link](https://codesandbox.io/s/y20wwly9pz)
