---
myid: 11
title: Dev Environment Setup
date: '2018-08-07'
description: 'Learn how to use create-react-app cli to setup boilerplate reactjs projects in this tutorial we will walkthrough step by step to run reactjs in our
local computer'
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---

So far we have seen some important concepts in react but we don't setup any development environment.

Let's install a react in our Local Computer.

Open your termnial and run below commands

```sh
npm install -g create-react-app
create-react-app myfrist-app
cd myfirst-app
```
Now open myfirst-app folder in your favorite text editor.

Delete everything in the *src* folder.

create a index.js file in the src folder.

first we need to import two libraries which are React and ReactDOM.

```javascript
import React from 'react';
import ReactDOM from 'react-dom';

//first component.

function Myfirst(){

    return <h1>Hello React dev</h1>;
}

//so far we are just created a component but we not attached
//it to the any dom node.

//let's attach it to a dom node.

//render method takes two arguments first
//one is  Component it need to
//render and second one is on which dom node it
//needs to attach this component.

ReactDOM.render(<Myfirst/>,document.getElementById('root'));

```
---

Now open your terminal and run *npm start* to fire the development server.

Thats it we are successfully completed  dev setup.