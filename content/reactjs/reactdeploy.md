---
myid: 17
title: Deploy a website
date:  '2018-08-09'
description: 'Learn How to deploy a react websites in github  easy way react tutorials  deploy a simple website/app using github pages'
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---

## How to deploy a reactjs website in Github pages?



*Make sure Your repository is already pushed to the github.*

1. Open your package.json file and add `homepage`  property.

```sh
"homepage":"https://yourusername.github.io/app-name"
```
---

2. Next we need to install a package called gh-pages

```bash
npm install --save gh-pages
```
---

3. Its time to add a deploy scripts in our package.json file.

```javascript

"scripts":{
 "predeploy": "npm run build",
 "deploy": "gh-pages -d build",
}

```

Now in your terminal run `npm run deploy `

Once you successfully deployed.


4. Setup source to gh-pages  branch.

click on settings tab and choose branch to gh-pages.

<img src="../../img/ghpages.png" alt="github-source" />