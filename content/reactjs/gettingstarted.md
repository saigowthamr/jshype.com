---
myid: 2
date: '2018-08-02'
title: Getting Started
description: 'In this tutorial you will learn about some basics about react components and what is jsx and virtual dom'
code: "
function HelloWorld(){\n
   \n
  return <h1>Hello World</h1>;\n
}
"
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---


<blockquote>
<p>Before You  start learning first read this post where i explained in very low very level
 <a href="https://codeburst.io/reactjs-for-beginners-67e57faeab84">Explaining React like i don't know anything</a></p>
</blockquote>


 No need to setup any dev environment for now Just use the Live Editor.


 # Simple React Component
```javascript
function HelloWorld(){
  return <h1>Hello World</h1>
}
```

what above code does is returning the h1 element.

Now Let me explain you What happens behind the scenes

 ## Jsx

 -  JavaScript Xml.

 - It means We are writing html inside the JavaScript.

 - You can still write React without Jsx it is just an Abstraction to make code more readable.


 This is how we write React without JSX.

```JavaScript
function HelloWorld() {
  return React.createElement(
    "h1",
    null,
    "Hello World"
  );
}
```



 ## Virtual Dom

 Virtual dom the name it says i'm not real it's just an object.

 By using the Virtual [dom](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model) object we are mutating the real dom or creating
 the real dom.

*Play it now*.

