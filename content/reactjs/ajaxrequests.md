---
myid: 15
title: Ajax request
date:  '2018-08-08'
description: 'Learn How to make ajax requests in react using  axios fetch library'
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---

## How to make Ajax Requests in React ?

- Axios is  a fetch based library.
- We are using Axios to make the ajax requests in React

For this first we need to install the axios library.

```bash
npm i --save axios
```
---

### Axios in Action

```javascript
import React from "react";
import axios from 'axios';

class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: null };
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/posts?userId=1')
    .then((res)=>{
      this.setState({data:res.data})
    })
  }

  getData() {
    if (this.state.data) {
      return <div>{this.state.data[0].body}</div>
    } else {
      return <div>Loading..</div>
    }
  }

  render() {
   return  this.getData();
  }
}
```
---

[componentDidMount](/reactjs/lifecycle) is a correct place to make the
ajax requests in react.

Once the response comes from the api we need to mutate the data property
with the response data.

 getData method  first checks if there is any data if it is show the
 data else show the loading.. Like we are  rendering [condtionally](/reactjs/conditionalrendering).



