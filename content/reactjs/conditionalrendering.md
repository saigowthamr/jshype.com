---
myid: 6
title: Conditional Rendering
date: '2018-08-06'
description: 'Conditional rendering helps to show elements dynamically to dom and
removed dynamically from dom'
code: "class Condtional extends React.Component{\n
    constructor(props){\n
        super(props);\n
        this.state = {show:false};\n
    }

    onShowhide(){\n
        this.setState({show:!this.state.show});\n
    }

    render(){\n
        return (\n
            <div>\n
          <h2>{this.state.show ? \"I'm visible\" : \"\"}</h2>\n
            <button onClick={this.onShowhide.bind(this)}>{this.state.show? \"Hide\":\"Show\"}</button> \n
            </div>\n
        )}}\n
"
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---

It means  we only render elements to dom  if the given condition is true.

example:

```javascript
if(true){
    return <h1>Me</h1>
}else{
    return <h1>Hit</h1>
}
```
### How to render  Conditonally  in Reactjs?

```javascript
class Condtional extends React.Component{
    constructor(props){
        super(props);
        this.state = {show:false}
    }

    onShowhide(){
        this.setState({show:!this.state.show})
    }

    render(){
        return (
            <div>
            {this.state.show ? "I'm visible" : ""}
            <button onClick={this.onShowhide.bind(this)}>{this.state.show? "Hide":"Show"}</button>
            </div>
        )
    }
}

```


