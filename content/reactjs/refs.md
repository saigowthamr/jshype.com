---
myid: 16
title: Refs
date:  '2018-08-08'
description: 'refs are used to take refernce of the element we are using arrow function in this example '
code: "
class AudioDetails extends React.Component{\n
    constructor(props){\n
        super(props);\n
    }

    componentDidMount(){\n
        console.log(this.audio)\n
        // you can get the complete reference of that audio element\n
        //check  your browser console \n
    }\n

 render(){\n
   var song = \"http://home.pilsfree.cz/quintrix/upload/mp3/Sean%20Paul%20-%20Crick%20Neck%20ft.%20Chi%20Ching%20Ching.mp3\"

     return (\n
         <div>\n
         <audio src={song} ref={(el)=>(this.audio = el)}  controls />\n
         </div>\n
        )\n
 }\n
}\n
"
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---

The name it says that if you use me i will give you back reference of
that element or node.

##  How to use Refs in React ?

```javascript
class AudioDetails extends React.Component{
    constructor(props){
        super(props);
    }

    componentDidMount(){
        console.log(this.audio) // you can get the
        // complete reference of that audio element
        //check your browsers console
    }

 render(){
   var song = "http://home.pilsfree.cz/quintrix/upload/mp3/Sean%20Paul%20-%20Crick%20Neck%20ft.%20Chi%20Ching%20Ching.mp3"

     return (
         <div>
         <audio src={song} ref={(el)=>(this.audio = el)}  controls />
         </div>
        )
 }
}
```
---
By using the callback function we are storing the reference of that node in  this.audio property so that we can control its behaviour.

