---
myid: 18
title: Comments
date:  '2018-08-16'
description: 'How to write comments in jsx learn more about comments in jsx,reactjs'
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---

## How to write Comments in JSX ?

```jsx
class Comments extends React.Component{

    render(){
        return (
            <div>
           { /* This is how we write comments in jsx;   */}
            <h1>This is h1</h1>

            </div>
        )
    }
}
```
---


- In css we write like this /*  */
- In jsx we write  {/* this is comment */}



