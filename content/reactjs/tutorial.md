---
myid: 1
date: '2018-08-01'
title: Introduction
description: 'Basics about reactjs How to use and build production ready single page apps by learning reactjs tutorial step by step interactive approach for beginners'
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---

# What is Reactjs ?

Reactjs is a Popular JavaScript library used to make user interfaces.It is maintained by Facebook and used by many tech companies in Production including Facebook.


## Advantages

 - Reusability.
 - Declarative
 - We can easily integrate one component with other.
 - In react everything is treated as a Component.
 - Virtual Dom




