---
myid: 7
date: '2018-08-06'
title: Events
description: 'Learn event handling and when we need to bind this keyword in reactjs'
code: " function Hitme(){ \n
        function onAlert(){\n
          alert('Hello');\n
        } \n
    return <button onClick={onAlert}>alert</button> \n
    }\n
"
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---

# Eventing handling

Event handling is little bit similar to Dom event handling

```html
<button onclick="somefun()">Fun</button>
```

But in  react we are using Jsx.

```html
<button onClick={somefun}>Fun</button>
```




In react if you want to pass any javascript expressions inside the jsx You need to
wrap with curly braces { }.


