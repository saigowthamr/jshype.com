---
myid: 14
date: '2018-08-08'
title: Children
description: 'If you write anything in enclosed jsx tags it will available in its child component named props.children  '
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---


In react we can use both opening and closing tags in Jsx like how we used
in the html elements.


```javascript
function Mychildren(props){
     return (
         {props.children}
     )

}

<Mychildren>
<div>Hello world</div>
</Mychildren>
```
---
What ever we passed inside the enclosing tags are accessed by the props.children property.


You can even pass Jsx in this enclosed tags.

```jsx
<Mychildren>
 <Name/>
 <Age/>
</Mychildren>
```


