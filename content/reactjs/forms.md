---
myid: 8
title: Forms
date: '2018-08-06'
description: 'How to create and submit forms using reactjs'
code: "class Simpleform extends React.Component {\n
    constructor(props){\n
        super(props);\n
        this.state = {name : ''};\n
    };

 onHandlechange(e){\n
     this.setState({name:e.target.value});\n
 }
 onSubmit(e){\n
     e.preventDefault();\n
     alert(`Your name is ${this.state.name}`);\n
 }
    render(){\n
      return  <form onSubmit = {this.onSubmit.bind(this)}>\n
        <input placeholder=\"Enter your name\" value={this.state.name} onChange={this.onHandlechange.bind(this)} />\n
        <button>submit</button>\n
        </form>\n
    }

}"
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---
Form handling in react

We need to update the state with currently  user entered data so that we need
to use onChange handler this is also called controlled component in react.

```javascript
class Simpleform extends React.Component {
    constructor(props){
        super(props);
        this.state = {name : ''};
    }

 onHandlechange(e){
     this.setState({name:e.target.value});
 }
 onSubmit(e){
     e.preventDefault();
     alert(`Your name is ${this.state.name}`);
 }
    render(){
       return <form onSubmit = {this.onSubmit.bind(this)}>
        <input value ={ this.state.name} onChange={this.onHandlechange(this)} />
        <button>submit</button>
        </form>
    }

}

```
