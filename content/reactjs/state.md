---
myid: 5
date: '2018-08-05'
title: State
description: 'Every statefull class components has there own internal state manges by the react where you can read and write'
code: "class Counter extends React.Component{\n
  constructor(props){\n
      super(props);\n
      this.state ={num:0}\n
  }

 onIncrement(){\n
     this.setState({num:this.state.num+1});\n
 }

 render(){\n
     return (\n
         <div>\n
         <h2>{this.state.num}</h2>\n
         <button onClick={this.onIncrement.bind(this)}>Inc</button>\n
         </div>\n
     )
 }

}"
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---

 State helps to hold Current app state and also we  can  mutate the  data present inside the components.There are even separate libraries to manage the state in react most popular one is Redux.

 we already know we can use state in only class Based components.

```javascript
function Name(props){
    return <h1>{props.myname}</h1>
}

class App extends React.Component{
    constructor(props){
        super(props);
        this.state={name:"React"}
    }
    render(){
        return(<div>
          <Name  myname={this.state.name} />
        </div>)
    }
}
```
---
In react we can use the single statefull component which is holding the all our app state
by using props we can pass the data to the stateless components Like in  above example.


final dom looks like
```html
<div>
<h1>React</h1>
</div>
```
---
In react we cannot mutate the state directly we need to use the setState method provided by react.

```javascript
this.state.name = "king" //not possible in react
```
---

There is only one way to mutate the state in react using setState.when state changes the components responds by re-rendering with updated state


### setState method

```javascript
class Counter extends React.Component{
  constructor(props){
      super(props);
      this.state ={num:0}
  }

 onIncrement(){
     this.setState({num:this.state.num+1});
 }

 render(){
     return (
         <div>
         <h2>{this.state.num}</h2>
         <button onClick={this.onIncrement.bind(this)}>Inc</button>
         </div>
     )
 }

}

```
---

setState doesn't gurantees you to update the state immediately
