---
myid: 3
date: '2018-08-04'
title: Components
description: 'What are Components in Reactjs diference between stateless functional components and statefull components'
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---


  Components are just a reusable piece of code and in react everything is treated a components for example header,sidebar in this website is just a components.

 In react there are two apporaches to write a Components and Component name should start with a
 capital letter other wise react treated it as a html elements.

 ### State less Components

- Components which are created using functions are called Stateless Components.

- it means there is no state object or any Lifecycle methods present
inside this components.

example :

```javascript
function Stateless(){
    return (
        <h1>Im stateless</h1>
    )
}
```
---
### Statefull Components

- Components which are created by using class syntax are called Statefull components.

- it means there is a internal  state object and  Lifecycle methods present inside this components.

example :

```javascript
class StateFull extends React.Component{

   constructor(){
     super();
     this.state ={name:"statefull"}
   }

  render(){
      return (
          <h1>{this.state.name}</h1>
      )
  }

}
```
---






