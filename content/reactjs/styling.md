---
myid: 12
title : Styling
date: '2018-08-07'
description: 'In normal *HTML5* we style the elements using  inline styles or internal styles or using external stylesheets.'
code: "function MyInlineComponent(){\n
    return (\n
     <div style={{color:\"red\" }}>\n
        <h1>How to use external style sheets in react</h1>\n
    </div>\n
    )
}
"
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---

In normal *HTML5* we style the elements using  inline styles or internal styles or using external stylesheets.


### How to use external style sheets in React ?

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';

function MyStyledComponent(){
    return (
     <div className="main">
        <h1>How to use external style sheets in react</h1>
        <p>To use external stylesheets in your component you
        need to import the stylesheet like how we import
        the components or libraries but  slightly change.
        <span>import './style.css'; thats it we just need to refer the path.</span>
        </p>
    </div>
    )
}


ReactDOM.render(<MyStyledComponent/>,document.getElementById('root'));
```
--

In html5 we are using `<div class="main">some data</div>`
 But in react we need to use the className instead of class.


### How to use Inline Styles in React ?


To use inline  styles we need to pass it as a object beacuse we are using jsx under the hood.
Don't confuse.

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';

function MyInlineComponent(){
    return (
     <div style={{color:"red" }}>
        <h1>How to use Inline Styles in React</h1>
    </div>
    )
}
ReactDOM.render(<MyInlineComponent/>,document.getElementById('root'));
```
--

### How to set styles conditionally?

Some time we need to add styles condtionally like disabled buttons or active navigations.


```javascript
class Condtional extends React.Component{
    constructor(props){
        super(props);
        this.state = {show:false}
    }

    onShowhide(){
        this.setState({show:!this.state.show})
    }

    render(){
        return (
            <div style={{color:this.state.show?"red":"blue"}}>
            {this.state.show ? "I'm visible" : ""}
            <button onClick={this.onShowhide.bind(this)}>{this.state.show? "Hide":"Show"}</button>
            </div>
        )
    }
}
```
---

This above component renders red color if show is true or else it sticks with blue color.




