---
myid: 9
title: Lists&Keys
date: '2018-08-06'
description: 'How to iterate over list of persons using map keyword'
code: " function Persons(){\n
  var persons = [\"king\",\"queen\",\"ninja\",\"evil\"];\n

  return (\n
      <ul>\n
      {persons.map((person,i)=><li key={i}>{person}</li>)}\n
      </ul>\n
  )\n
}"
logo: /img/reactlogo.svg
thumbnail: /img/reactjs.png
---

Suppose we have a persons list or array of persons.

```javascript
var persons = ["king","queen","ninja","evil"];
```
---

## How to render this persons array in react?

we need to use the es6 map method.

```javascript
function Persons(){
  var persons = ["king","queen","ninja","evil"];

  return (
      <ul>
      {persons.map((person,i)=><li key={i}>{person}</li>)}
      </ul>
  )
}
```
---

## Keys

We need to pass the key attribute to list items because react needs to track
the changes of that element
