---
myid: 3
title: Getting Started
date: '2018-08-22'
description: "Learn how to install angular cli and tooling setup "
logo: /img/anglogo.png
thumbnail: /img/angular-logo.png
---

In this tutorial we will learn  about.

1. how to install the angular cli.
2. how to create a new project.


**Angular cli** is a command line interface which helps us to generate the new projects.



Let's install the Angular cli.

**Requirements**

- Nodejs version 8 or greater.

Open your terminal and run below command.

```bash
npm install -g @angular/cli
```

<blockquote>Once it's installed run ng -v  if version number is shown you are successfully installed
</blockquote>


#### Create a new project using Cli

```bash
ng new myfirst-app
```

Once you run the above command angular cli generates the new project in the myfirst-app folder.


It's time to run the development server

```bash
ng serve //it helps to run the development server.
```

Now open localhost:4000 in your favorite browser.

You can see a angular logo with some links.
