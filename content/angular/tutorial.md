---
myid: 1
title: Introduction
date: '2018-08-22'
description: "How to angular is a step by step tutorial to learn basics about framework,typescript and it's advantages"
logo: /img/anglogo.png
thumbnail: /img/angular-logo.png
---

Angular is a javascript framework used to build modern enterprise level web apps.Everyone is loving angular because of it uses the typescript for strong type system.

Angular is a complete rewrite of angularjs.

**When Angular is relased ?**

- Final version is relased on  14 September 2016.


## Advantages of Using Angular

- Strong type system.
- Good tooling infrastructure.
- Simple and declarative templates.
- You can build native mobile apps using Ionic,cordava and also desktop based apps.

### Who uses Angular

1. Google express
2. Code.gov
3. Udacity
4. Udemy

Many more companies uses angular in production.




