---
myid: 4
title: Exploring
date: '2018-08-23'
description: "Folder structure used in angular projects and root folder app is present inside the src directory learn it online"
logo: /img/anglogo.png
thumbnail: /img/angular-logo.png
---

In last tutorial we successfully created a new angular app.

In this tutorial we will learn about folder structure used in the
angular.

Open myfirst-app folder in your favorite code editor have you seen
app folder which is present inside the src folder.


Four files present inside the app folder which are

```
- app.component.html
- app.component.css
- app.component.ts
- app.module.ts
```

Open  app.module.ts file

```javascript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

This also is called as root app module in angular where we need to import the modules and declare the components so that angular has aware of your components and modules because angular first runs the app.module.ts.


Now open app.component.ts.

```javascript
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
```

@Component() which is a decorator method provided by the angular.
where we need to declare the three things.

- **selector**: it means the name of the template.
- **template**:  we need to refer the template url for app component.
- **styleUrls**: we need to refer the css url for app component.


Everytime you create a new component  you need to specify these three
things.

At last open index.html

```html
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>MyfirstApp</title>
  <base href="/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body>
  <app-root></app-root>
</body>
</html>
```

Have you seen app-root is added inside the body where  it is declared inside the app component selector url it means anything you
write inside the app-root template goes inside the body tag.







