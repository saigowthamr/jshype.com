---
myid: 9
title: For Loop
description: 'Learn how to  write for loops in javascript for of loop for in loop
what is a loop and how it works'
date: '2018-08-10'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---


## What is loop ?

If Same type of  task is done repeatedly is called loop like we play same song repeatedly.


### For Loop example

How to  Print 1-100 numbers using For Loop?.

```javascript
for(let i=0; i<=100;i=i+1){
  console.log(i)
}
```
---

Let's talk about what is the use of above code.

for loop takes three optional expressions .

1) Initialization: we are initializing our value - (let i=0);
2) Condition: On which condition iteration if the condition is true then the loop continues otherwise loop stop running.(i<=100);
3) Final expression : How loop increments/ Decrements in each iteration (i=i+1)
Inside the for loop, we are logging the value on each iteration.

Without for loop, we need to write 100 numbers manually 1 by 1 using  for loop, it only takes 2 lines of code.

`Try it on below console`
<iframe src="https://jsconsole.com/?%3Awelcome" style="height:200px;"></iframe>