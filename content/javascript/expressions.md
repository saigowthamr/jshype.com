---
myid: 4
title: Expressions
description: 'Learn how to  write expressions in javascript expressions in depth
function expressions'
date: '2018-08-09'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---

### Expressions which are evaluated by the javascript engine and produce a desired value.

```javascript
1+2; // expression evalutes the value 3

{id:2,data:"loading"};

[2,3,4,4,4]; //expression

"javascript is rich";

```

`1+2` evaluates and produce 3