---
myid: 16
title: Function Expressions
description: "Learn how to  write function expressions in javascript this key word in function expressions  what is function expressions where to  uses function expressions in javascript beginner's guide tutorials parameters arguments"
date: '2018-08-10'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---


```javascript
const myName = function(name){
                  console.log(name)
                }

    myName("guest") // guest
```
---
The right side of the assignment is called as expression.
Normal function expressions are anonymous.
We need to assign the expression to the variable.


## IIFE

Immediately Invoked Function expression.

It means functions are invoked immediately once it is defined.
Self executing functions.

```javascript
(function () {
    console.log('IIFE');
})();
//outputs "IIFE"
```