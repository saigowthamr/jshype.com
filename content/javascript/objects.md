---
myid: 18
title: Objects
description: "Learn how to  use objects in javascript what is  a object javascript  update objects using dot notation delete objects object orientied programming using javascript"
date: '2018-08-10'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---



<mark>We can define  objects in JavaScript using object literal.</mark>

```javascript
const obj = {};

console.log(typeof obj); // output is Object
```

**Objects are collection of key value pairs.**

```javascript
const data={
            name:'javascript',
            frameworks:['reactjs','vuejs','angular']
         }
```
---

Left side is key  or Property right side is value.

We can access the values inside the object using there keys.Two different ways to access the values .

### Dot notation

```javascript
console.log(data.name);
//output is  javascript

```
---
### Using Brackets

```javascript
console.log(data['name']) // javascript
```
---
### How to Update a values in Objects?

- We can use both dot notation and Brackets to update the values.


```javascript
data.name='guest';
console.log(data.name) // output is guest
data[‘name’]='king';
 console.log(data[‘name’]) // output is king

```
---


### How to delete a key value pairs from Object?

Delete operator helps us to remove completely both key and value.

```javascript
const person={
          id:1,
          school:"wsc"
         };
delete person.school; // deleted school

console.log(person);

//output is {id:1}
```

`Try it on below console`
<iframe src="https://jsconsole.com/?%3Awelcome" style="height:400px;"></iframe>