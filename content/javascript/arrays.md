---
myid: 19
title: Arrays
description: "Learn how to update arrays  in javascript what is  a array  push ,pop shift, unshift. using dot notation object orientied programming "
date: '2018-08-19'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---



- It helps us to handle the collection of data.
- We can access items in the array using its location.
- Arrays Location are always start from 0.


We can  create an Array using Array Literals.

```javascript
let  arr =[ ];
```

 ```javascript
let  arr=[100,200,300];
 console.log(arr[0]); // 100
 console.log(arr[1]); // 200
 console.log(arr[2]); // 300

```
---

## How to Update the Items in Array?

 - Updating items in the array by using the location of the item.

```javascript
let  arr=[100,200,300];
     arr[0] = 50;
     console.log(arr); // [50,200,300];
```
---
## How to remove Items from  the  Array?
  - JavaScript gives us two different methods to remove the items
  From the array.

**Pop**:Removes the last item from the array.

**Unshift**:Removes the first element from the array.

```javascript
let  arr =[100,200,300];
arr.pop()// removes  300 from the array.
console.log(arr)// [100,200];
arr.shift() // removes 100 from the array.
console.log(arr) //[200]
```

---
## How to Add new  Items to the  Array?

 - JavaScript gives us two different methods to add the items
  From the arr.

**Push**:Adds the  item to the end of an array.

**Unshift**:Adds the element to the  beginning of an array.


```javascript
let arr =[100,200,300];
arr.push(400)// adds  400 to end of the array.
console.log(arr)// [100,200,300,400];
arr.unshift(50) // add 50 to beginning of the array.
console.log(arr) //[50,100,200,300,400]
```

---

## How to Find Number of items in Array?

- To find the number of items in array we need to use array.length.

```javascript
let arr=[10,11];
console.log(arr.length); // 2
```
---

## How to Iterate  elements from the array?

They are Different ways to iterate elements present in the array.

For Loop is most commonly used to iterate the  elements from the Array.


```javascript
let arr = [100,200,300,400,500];

for(let i=0;i<arr.length;i++){
   console.log(arr[i]);
}
```

