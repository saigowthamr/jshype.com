---
myid: 11
title: While Loop
description: 'Learn how to  write While Loops in javascript for of loop for in loop
what is While loop where While Loop uses in javascript'
date: '2018-08-10'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---

Let’s Solve the same problem using While loop.
we solved in [forloop](/javascript/forloop)

*How to  Print 1-100 numbers using For Loop?*

```javascript
let n=1;
while(n<=100){
 console.log(n);
 n=n+1;
}
```

<mark>
"while loop runs only if condition is true.Otherwise it doesn't enters inside the loop".
</mark>

1. We first need to initialized a value to the variable n with 1.
2. while loop only takes single expression which is Condition (n<=100).
3. It  runs up to n value is equal to the 100.
4. On line 2 we are incrementing n on each iteration.


`Try it on below console`
<iframe src="https://jsconsole.com/?%3Awelcome" style="height:200px;"></iframe>