---
myid: 1
title: Introduction
description: Javascript is a  high level programming language used widely and it is most misunderstood programming language learn javascript.
date:  '2018-08-08'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---

### Why JavaScript?

JavaScript is a high level programming language used everywhere from the server to the browser, Machine Learning, and Internet On things(IoT).By using JavaScript you can make software for both Windows and Mac, Android & IOS apps, Web apps.


### User Interaction

By using JavaScript you can change the behaviour of the website by interacting with
the dom api.


### Easy to start
Javascript is dynamically typed language it means variables do not need to a type specified.No need to download any extra compilers.Just Code Editor and a Browser all you need.
