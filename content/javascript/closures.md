---
myid: 17
title: Closures
description: "Learn how to  use closures in javascript what is  a closure function javascript beginner's guide tutorials"
date: '2018-08-10'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---

Closures are Very Interesting in JavaScript.

When we nest a one function inside the other function we can still
Use the variables and parameters of the outer function.

## Lexical Scoping

```javascript
function outer(name){
   let name=name;
   function inner(){
     console.log(name);
  }
}
```

Have you seen in above inner function can have still access the
Variables we declared in the Outer function.


Example 2:

```javascript
function outer(){
    let name;
  return  function (myname){
     name=myname;
     return name;
    }
}
```

```javascript
let me = outer(); // outer returns anonymous function
 So that now me is holding the anonymous function
me(‘javascript’); //  output is javascript
```
In closures we can have still access to the outer function variables and
Its parameters even though it is returned.

