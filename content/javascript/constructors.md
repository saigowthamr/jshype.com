---
myid: 21
title: Constructors
description: "Learn how to write constructors and what is a new keyword in constructor where to invoke new keyword"
date: '2018-08-10'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---

Constructors helps us to create the objects


Example:

```javascript
function Person(name,id){
    this.name=name;
    this.id=id
}

var me = new Person('guest',1);

console.log(me) // {name:'guest',id:1 }

```
---


-  Constructor function name should starts with a capital letter.
-  In constructor we need to use this.property = value
-  To invoke a constructor function we need use new keyword.
-  It creates the instance of the person.

