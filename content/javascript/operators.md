---
myid: 5
title: Operators
description: 'Learn how to  write expressions & operators work in javascript expressions and operators logical operators  unary binary operators,Assignment Operators,Arthimetic Operators in depth with examples function expressions'
date: '2018-08-09'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---

In JavaScript we have both unary and binary operators.

####Binary operators

- It requires two operands

2 + 3

`operand operator operand`

2 and 3 are operands + is operator.

##### Unary operator.

It requires single operand

`x++`    `++x`



### Assignment Operators.

It means we are assigning a value  present in the right side is assigned  to the left side side variable.

x = 10;

assigning 10 to the variable x.



### Comparing Operators.

It compares the two values present on the left hand side and right hand side and
return boolean either true or false

In javascript we have two types of operators are used for comparsion.

####  Equality (==)

`1 == 1` // outputs true

if we compare string with a number often javascript does type coercion

#### Coercion

It means if tries add Number  1 and string 1

example : `1+"1"` // outputs string "2"


Because javascript tries to add  it as a number it is not possible so it converts the
number 1 into string and add again now it become      `"1"+"1" ` outputs `"2"`

Same thing happens when we compare using Double equals (==) it does the type
coericon.

```javascript
 1 == "1" //  true.
```
---
#### Strict Equality (===)

Strict Equality it means they don't do coercion

Example:
if we compare  number 1 and string "1" using (===);
```javascript
1  === "1" // false
```

Why beacuse  left hand side 1 is a type of Number and right handside 1 is a type of
a string so its returns false.


### Arthimetic Operators

Arthimetic operators helps to perform some desired operations  like its take
the  operands and produce the numeric value such as addtion(+),subtraction(-),multiplication(*),divison(/),Modulo(%);


#### Addition (+)

Addition operator helps to  add to numbers or we can concate strings using + operator.if we try to add number and strings final output becomes string.

Examples:

```javascript
1+2 // 3
 2 + "Id" // "2 Id"

""+1 // "2"
```
---

#### Subtraction(-)

Subtraction operator helps to subtract between two operands if other operand is not a number it tries
convert it as a number.

Examples:

```javascript
1-2 // -1
 2 - "1" // 1

1-true// 0
```
---

#### Multiplication (*)

* operator helps to do Mutliplication of operands if  other operand is not a number it tries
convert it as a number.

Examples:
```javascript
1*"2" // 2
2*2 // 4
```
---

#### Divison (/)

/ helps to us to do the divison between 1st operand and 2nd operand if other operand is not a number it tries convert it as a number.There is  no integers in Javascript however all numbers are floating point in javascript.

Examples:
```javascript
 5/2 // 2.5
 6/3 === 2 // true
 2/0 // infinity
 0/0 // NaN - not a number
```
---

#### Modulo(%)

% operator helps to  do the divison of the first operand and second operand and returns it remainder.


Examples:
```javascript
 4%2 //  2 divided by 4 and produces remainder 0

 10%3  // 3 divided by 10 and produces remainder 1
```

Some other are Increment (++) decrement (--).


##Logical operators

Logical operators are used manily to perform boolean Algerba when they are logically
return boolean value.Some times if we used in the loops it gives values of that specified operands.

 - ! not operator.
 - && and operator.
 - || or operator.

#### Not operator(!)
 - Not true means false
 - Not false means true.


Example :
```javascript
 console.log(!true);//false
 console.log(!false); //true
```
---

#### And Operator(&&)

If you want to join in our party your age should be 18 and You should wear a party dress.
```javascript
let age =18;
let dressCode =['normal','party'];

if(age >=18 && dressCode[1]=='party'){
    console.log('Welcome to our party');
}else{
  console.log('you are not able to come');
}

//output is 'Welcome to our party'

```
---

#### OR Operator(||)

```javascript
  console.log(true || false); //true
  console.log(false || true); //true
  console.log(0 || 1); //1
  console.log(1 || 2); //1
  console.log('' || 'script');// script
```
---
OR Operator only prints the  truthy values .

In second example  || operator first sees the falsy value so that it moves to the  true and logs true in the console.

In fourth example || operator first sees the 1 it is truthy value so that
Or operator doesn’t go for 2.

#### Ternary  operator(?)

  condition ? val : val
```javascript
let  num =  1;
(num == 1 ) ? console.log(1):console.log(0);

// Output is 1 because conditon evaluates and returns true
(num == 0 ) ? console.log(1):console.log(0);

// Output is 0 because conditon evaluates and returns false
```
---
If condition is true prints 1 else prints 0.


## Type operator

It is used check the type of the data like strings,numbers,functions,objects.

```javascript
typeof 1 // number
typeof "javascript" //string
typeof {id:1,name:"guest"} // object
```