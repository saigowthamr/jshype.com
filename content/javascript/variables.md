---
myid: 6
title: Variables
description: 'Learn how variables work in javascript let,const and var symbols easy way in javascript'
date: '2018-08-09'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---


Variables helps us to store the data where often we can manipulate or change the
data present inside the variables.


Variables can be declared by using var,let,const.

examples:

```javascript
var age = 11;
let name = "guest";
const year = 2018;

age = age+2; // gives the output 13

```
---

## What is difference between let and var?

let allows to do block scoping it means you can only access that variable inside
the block.


What is const?

const it means constant data can be only assigned once we can't reassign the data.

Example :

```javascript
const num = 20;
num = 300; // not possible
```

