---
myid: 15
title: Call bind apply
description: "Learn how to  write functions in javascript  call bind apply this key word in functions.Difference between parameter and arguments"
date: '2018-08-10'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---


## Call

Call method takes two args first one is to which object this needs to refer and the second argument is there any arg need to that function. But in our dummy function, we don't need any arguments.



```javascript
let obj={
    id:23,
    grade:"c"
}

function getId(num){
    return this.id+num;
}

getGrade.call(obj,2);// 25
```
---


## Apply

apply method also takes two args first one is to which object this needs to refer and the second argument is there any argument need to that function. but in apply method we need to pass second argument as a array.



```javascript
let obj={
    id:23,
    grade:"c"
}

function getId(num){
    return this.id+num;
}

getGrade.apply(obj,[2]);// 25
```
---

## Bind

Apply and Call method invokes the function immediately but Bind method doesn’t invokes the function immediately instead of it returns the new function.



```javascript
let obj={
    id:23,
    grade:"c"
}

function getId(num){
    return this.id+num;
}

let newGrade= getGrade.bind(obj,2);

newGrade() // // 25
```
---

`Try it on below console`
<iframe src="https://jsconsole.com/?%3Awelcome" style="height:500px;"></iframe>