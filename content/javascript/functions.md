---
myid: 13
title: Functions
description: "Learn how to  write functions in javascript  call ,bind ,apply methods and this key word in functions"
date: '2018-08-10'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---


Functions are key to the programming and we can reuse the functions.

Every function in JavaScript starts with a keyword function.


#### Function Declaration👷🏼‍♀️

```javascript
function add(num1,num2){
 return num1+num2;
}
```


We called it as a function definition or function statements/declaration.

#### What above function does ?

a function with a name add takes two parameters and returns the addition of the two parameters.So By defining a function can’t do any work to make functions work we need to invoke it/calling a function is called function invocation.

#### How can  we call a function in JavaScript?

- They are 4 different ways to call a function?

#### Function Invocation 👮🏼

```javascript
 add(1,2) // output is 3
```
we are passing two arguments to the function which are 1,2 so we get an output addition of  1 and 2 which is 3.

What the hell arguments/parameters have you confused why I'm calling in different names in the same function.

Let’s clear the confusion now.

**Parameters**: which are passing to the function at the time of declaration.
Parameters are named variables which are a,b in our add function.

**Arguments**: Which are passing to the function at the time of invocation.Arguments are the values which are 1,2 in our add function.


## What is return keyword😛?

 - return is a keyword it helps to stop✋ the running function.