---
myid: 10
title: IF Loop
description: "Learn how to  write If Loops in javascript for of loop for in loop
what is If loop where If Loop uses in javascript beginner's guide tutorials"
date: '2018-08-10'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---


<mark>if loop runs on the basis of  truthy values.</mark>

- Example 1:

```javascript
 let enter=true;
 let n=1;
if(enter){
 console.log(n);
}
```


In above code we  declared a variable  enter and intialized with true so it enters inside the block and logs output 1.


### How if loop works in daily life?

- In our day to day life, we are making decisions like if time is 9 am I need to eat breakfast.if time is 12 am I need to eat lunch.

#### Have you thinking about if else loop.

- if time is 9 am I need to eat breakfast else I need to do some other things(brush, bathing,etc..).


Example 2:

```javascript
let time = '9am';
if(time === '9am'){
  console.log('i need to eat breakfast')
}
else{
  console.log('i need to brush')
 }
```
If the time is 9am then logs i need to eat breakfast. else  i need to brush.

`Try it on below console`
<iframe src="https://jsconsole.com/?%3Awelcome" style="height:200px;"></iframe>