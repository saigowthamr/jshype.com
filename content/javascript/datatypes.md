---
myid: 3
title: Data Types
description: 'Learn how datatypes work in javascript numbers,string and objects symbols boolean'
date: '2018-08-09'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---

The name it tells the type of the data which we used to represent is called
datatypes .

Example:
```javascript
"name"
12 //number
```
---
Types of often divided into two categories Primitive types and Reference types.

## Primitive Types
- String
- Number
- Boolean
- Undefined
- Null


#### Number

Number is a 64-bit floating-point format defined by the IEEE 754 standard.It means
you can represent as big as you can as ±1.7976931348623157 × 10308 and as small as ±5 × 10−324.This no integer type which we see in other languages like c and java.

Examples:

```javascript
1
234
13.40393

```
---
#### String

- Strings are used Unicode characters in JavaScript which means each character takes 16bits or 2bytes.
- A String type in javascript is represented by using  double quotes("") or single quotes("") both are accepted.

Examples:

```javascript
'hello'
"hello"
```
---

### How to write multiline strings in Javascript?

Examples:

```javascript
'Today im \nlearning javascript'
```
\n prints the string in new line like in above example `learning javascript prints on new line`.

Copy the above string and paste it on below console and hit enter.
`Try it`.

<iframe src="https://jsconsole.com/?%3Awelcome" style="height:200px;"></iframe>


#### Boolean

Boolean is a type which often have true or false. where you can add or subtract booleans in  javascript true has a number 1 false has number 0.

Examples
```javascript
true;
false;
true-false;
true+true;
```
---
<iframe src="https://jsconsole.com/?%3Awelcome" style="height:200px;"></iframe>

##Falsy values in JavaScript

- '' // empty string literal single quoted
- "" // empty string literal double quoted
-  0
- -0
- NaN // not a number
- false
- null
- undefined


#### Undefined

You can see undefined mainly in javascript where you declared  a variable but not
intialized with any value to it.

#### Null

Null means nothing or nill.


## Reference Types

Refrenced types which are belong to the type Object.

#### Objects

Objects can be represented in javascript by using collection of key value pairs in javascript.

Examples

```javascript
{
  name:"guest",
  age:01
}
```
---

In above example name is the key and value is the "guest"


#### Arrays

Arrays are also treated as a Object type  in javascript but they works differenlty compared to the arrays.

Formal definition: Array is collection or group  of characters or values.

```javascript
[2,3,4,5]
```
---
#### Functions

Functions are key to the programming and we can reuse the functions.
Every function in JavaScript starts with a keyword function.

Let's write our first function we already know every function starts with a keyword function.

#### Function Declaration👷🏼‍♀️

```javascript
function add(num1,num2){

 return num1+num2; // function body

}
```
---

This often  called with different names function definition or function statements don't
confuse.


Declaring a function can't do anything we need to invoke the function.

## How to Invoke a functions?

To invoke a function we need to type name of the function and () pranthesis.

```
add(1,2);
```

To know more refer  [functions](/javascript/functions) topic.




