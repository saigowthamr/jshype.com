---
myid: 12
title: Hoisting
description: "Learn what is hoisting in javascript  call bind apply this key word in functions. remember hoisting  is not something technically moved to top of the programs"
date: '2018-08-10'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---


<mark>Hoisting is not something functions and variables are moving to the top.
Actually, they are not moving technically anywhere.</mark>

The thing happens at the time of function declaration the complete function is sitting in the memory.

For variable declaration, JavaScript engine initializes with the value "undefined" at the time of creation.i used debugger to stop the running code at line 6.

![hoisting in javascript](../../img/hoisting.gif)

For example

- Have you Observed one thing in above image JavaScript is already initialized with value 'undefined'?

- Whenever JavaScript engine runs the line 6 then it updates the undefined to 'hoisting'.
- for functions, the full function is added to the memory space.
That is the reason we can invoke the functions anywhere in the file but not variables.
- for variables, we only used once it is declared first.


let key word solves the problem of hoisting.