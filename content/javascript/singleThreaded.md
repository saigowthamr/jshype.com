---
myid: 8
title: Single Threaded
description: 'Learn  single threaded synchronous in  javascript what is threading in javascript easy way in javascript'
date: '2018-08-09'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---


At any point of time JavaScript is running only one thing.

### Synchronous

Line by line it means one after another

example:

```javascript
var num = 1;

var smile = "ha ha 🙂";

console.log(num); // first prints 1
console.log(smile);// second ha ha
```
---
In above code first prints  1 and next "ha ha" .it doesn't run smile first and num second.

`Try it in below console.`

<iframe src="https://jsconsole.com/?%3Awelcome" style="height:200px;"></iframe>





