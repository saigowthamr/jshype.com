---
myid: 7
title: Scope
description: 'Learn how scope work in javascript global scope and local scope behaviour in beginners javascript'
date: '2018-08-09'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---

Scope means where to look  in javascript.
Because we declared the variables at different location in the Program.


1. Global Scope
2. Local  Scope

## Global Scope

 Global scope means  you can access me anywhere in the  program.

 example :

```javascript
var me = "king"; //global scope.

function getName(){
    return me; // you can access it any where
}

```

## Local Scope

- If we declare any variables inside the functions.
- The variables are only accessed within that function.
- In javascript if we declared any variables inside the function we often called It as private variables.

```javascript

function getName(){
    var me = "king"; // local scope.
}

console.log(me) //  output is error
```


## Execution Context

when we run javascript code it creates a execution context in global for
functions javascript creates it own execution context.


## Call Stack

A call stack is a place which keeps track the code. when we run a multiple functions.
it will keep track currently which function is running if the function is executed
it removes from the call stack.

to know more about   [Call stack and Execution](http://davidshariff.com/blog/what-is-the-execution-context-in-javascript/)