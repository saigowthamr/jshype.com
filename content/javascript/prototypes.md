---
myid: 20
title: Prototypes
description: 'Learn protoypes in JavaScript beginner tutorials guide javascript prototypal inheritance explained in easy way'
date: '2018-08-10'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---


- It means Objects which are inherited from the other Objects.
- In JavaScript every object has  a hidden property called prototype.
- You can add your own properties to the prototype by using Object.prototype.


Let me show you in example.

example :

```javascript
var obj = {
  id:1,
  learning:true
}

var me = Object.create(obj);

console.log(me); //{ } empty object
cosnole.log(me.id); //1
console.log(Object.getPrototypeOf(me) === obj); //true
```
---
- Object.create method creates the prototype of object.

- In above code we created a me object with properties inherited from the obj.

- if we log console.log(me.id) the property id is not present in the me
object itself but it is present in its prototype chain `me.__proto__` which is inherited from the obj this is called prototypal inheritance.

`Try it on below console`
<iframe src="https://jsconsole.com/?%3Awelcome" style="height:200px;"></iframe>









