---
myid: 14
title: this keyword
description: "Learn how this  keyword works in javascript  why this keyword is changing it value"
date: '2018-08-10'
logo: /img/jslogo.png
thumbnail: /img/js.jpg
---


this keyword  is not always referring the same thing in JavaScript.

If we call a this keyword inside the function this will be refer to the
Global window object.

It means the value of the this keyword depends upon where we are
calling it.


```javascript
let obj ={name:' lol',
          getName:function(){
                   console.log(this.name);
                }
        }
console.log(obj.getName()) // output is lol

```
In above example this keyword refers to obj
If we use this inside the object this refers to that object.
it means this = obj.


This keyword in functions

in functions this keyword

```javascript
function keep(){
    console.log(this)
}

keep() // window object
```
When we Log a this keyword inside the function it refers to the global object by default.

if we need to change the default behavior of the function we need to tell the function at the time of invoking where this keyword needs to refer.



<mark>If we use strict mode this is undefined in functions.</mark>

