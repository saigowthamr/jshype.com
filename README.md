# What is Js hype?

This a website related to  tutorials and articles about JavaScript & Web development.


## Install

Make sure that you have the Gatsby CLI program installed:

```sh
npm install --global gatsby-cli
```

And run from your CLI:

```sh
git clone git@github.com:saigowthamr/jshype.com.git
```

Then you can run it by:

```sh
cd jshype.com
gatsby develop
```

This a private repository